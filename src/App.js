import {useState, useEffect} from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar.js';
import AdminDashboard from './pages/AdminDashboard.js';
import Error from './pages/Error.js';
import Home from './pages/Home.js';
import Logout from './pages/Logout.js';
import Orders from './pages/Orders.js';
import Products from './pages/Products.js';
import Profile from './pages/Profile.js';
import Register from './pages/Register.js';
import SingleProduct from './pages/SingleProduct.js';
import Users from './pages/Users.js';
import './App.css';
import {UserProvider} from './UserContext.js';


function App() {
  const [user, setUser] = useState({
    id: null,
    firstName: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  const [loading, setLoading] = useState(true);
  
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          firstName: data.firstName,
          isAdmin: data.isAdmin
        });
        setLoading(false);
      }
      else {
        setUser({
          id: null,
          firstName: null,
          isAdmin: null
        });
        setLoading(false);
      }
    })
    .catch((error) => {
      console.log('Error fetching user data:', error);
      setLoading(false);
    });
  }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser, loading, setLoading}}>
      <Router>
        <AppNavbar/>
        <Routes>
          <Route path="*" element={<Error />} />
          <Route path="/adminDashboard" element={<AdminDashboard />} />
          <Route path="/" element={<Home />} />  
          <Route path="/logout" element={<Logout />} />
          <Route path="/orders" element={<Orders />} />
          <Route path="/products" element={<Products />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/register" element={<Register />} />  
          <Route path="/products/:productId" element={<SingleProduct />} />
          <Route path="/users" element={<Users />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}


export default App;