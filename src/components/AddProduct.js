import {useState, useEffect} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2'


export default function AddProduct({fetchData}){
    const [image, setImage] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [stock, setStock] = useState("");
    const [showForm, setShowForm] = useState(false);
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        setIsActive(image && name && description && price && stock);
    }, [image, name, description, price, stock]);

    const openForm = () => {
        setShowForm(true);
    }

    const closeForm = () => {
        setShowForm(false);
        setName("");
        setDescription("");
        setPrice(0);
        setStock(0);
    }

    const addProduct = (event) => {
        event.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/products/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                image: image,
                name: name,
                description: description,
                price: price,
                stock: stock
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data.message) return;
            if(data.status === "OKAY"){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product added successfully!"
                });
                closeForm();
                fetchData();
            }
            else if(data.status === "INVALID_PRICE"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Invalid price value."
                });
                closeForm();
            }
            else if(data.status === "INVALID_STOCK"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Invalid stock value."
                });
                closeForm();
            }
            else if(data.status === "EXISTING_PRODUCT"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Product already exists."
                });
                closeForm();
            }
        });
    }

    return (
        <>
            <div className="text-center mt-3">
                <Button variant="primary" size="sm" onClick={() => openForm()}>Add Product</Button>
            </div>
            <Modal show={showForm} onHide={closeForm}>
                <Form onSubmit={(event) => addProduct(event)}>
                    <Modal.Header>
                        <Modal.Title>Add Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId="productImage">
                            <Form.Label>Image Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter Image Name" value={image} onChange={event => setImage(event.target.value)} required/>
                        </Form.Group>
                        <Form.Group controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter Name" value={name} onChange={event => setName(event.target.value)} required/>
                        </Form.Group>
                        <Form.Group controlId="productDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" placeholder="Enter Description" value={description} onChange={event => setDescription(event.target.value)} required/>
                        </Form.Group>
                        <Form.Group controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" placeholder="Enter Price" value={price} onChange={event => setPrice(event.target.value)} required/>
                        </Form.Group>
                        <Form.Group controlId="productStock">
                            <Form.Label>Stock</Form.Label>
                            <Form.Control type="number" placeholder="Enter Stock" value={stock} onChange={event => setStock(event.target.value)} required/>
                        </Form.Group>
                    </Modal.Body>  
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeForm}>Close</Button>
                        <Button variant="success" type="submit" disabled={!isActive}>Submit</Button>
                    </Modal.Footer>           
                </Form>
            </Modal>
        </>
    )
}