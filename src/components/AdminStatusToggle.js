import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function AdminStatusToggle({user, fetchData}){
    const setToUserToggle = (userId) => {
        console.log(userId);
        fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/user`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data.message) return;
            if(data.status === "OKAY"){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "User is set to regular user successfully!"
                });
                fetchData();
            }
            else if(data.status === "ALREADY_USER"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "User is already a regular user."
                });
            }
            else if(data.status === "INVALID_USER"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "User does not exist."
                });
            }
        });
    }

    const setToAdminToggle = (userId) => {
        console.log(userId);
        fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/admin`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data.message) return;
            if(data.status === "OKAY"){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "User is set to admin successfully!"
                });
                fetchData();
            }
            else if(data.status === "ALREADY_ADMIN"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "User is already an admin."
                });
            }
            else if(data.status === "INVALID_USER"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "User does not exist."
                });
            }
        });
    }

	return (
        user.isAdmin ?
            <Button variant="success" size="sm" onClick={() => setToUserToggle(user.id)}>SetToUser</Button>
    	:	
            <Button variant="danger" size="sm" onClick={() => setToAdminToggle(user.id)}>SetToAdmin</Button>
	)
}