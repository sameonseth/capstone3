import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Banner({data}){
	const {parent, title, content, destination, label} = data;

	return (
		<Row className="min-vh-92" id={`${parent}-banner`}>
			<Col xs={12} id={`${parent}-banner-text`} className="d-flex flex-column align-items-center justify-content-center">
				<div id={`${parent}-banner-title`} className="text-center">{title}</div>
				<div id={`${parent}-banner-subtitle`} className="text-center">{content}</div>
				<Button as={Link} to={destination} className="primary-button">{label}</Button>
			</Col>
		</Row>
	)
}