import {useState, useEffect} from 'react';
import {Row, Col, CardGroup} from 'react-bootstrap';
import PreviewProducts from './PreviewProducts.js';


export default function FeaturedProducts() {
	const [previews, setPreviews] = useState([]);
	const [noProducts, setNoProducts] = useState(false);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(data.message) return;
			if(data.status === "EMPTY_PRODUCTS") setNoProducts(true);
			else{
				setNoProducts(false);
				const numbers = [];
				const featured = [];
				const generateRandomNums = () => {
					let randomNum = Math.floor(Math.random() * data.length);
					(numbers.indexOf(randomNum) === -1) ? numbers.push(randomNum) : generateRandomNums();
				}
				for(let i=0; i < (data.length >= 3 ? 3 : data.length); i++){
					generateRandomNums();
					
					featured.push(
						<PreviewProducts data={data[numbers[i]]} key={data[numbers[i]]._id}/>
					)
				}
				setPreviews(featured);
			}
		})
	}, []);

	return (
		<Row className="min-vh-92">
			{noProducts ?
				<h2 className="text-center my-5">No Products Available</h2>
			:
				<>
					<Col xs={12}>
						<p className="text-center my-5 main-title">Check Our Drip</p>
						<CardGroup className="d-flex justify-content-center gap-5">
							{previews}
						</CardGroup>
					</Col>
				</>
			}
		</Row>
	)
}