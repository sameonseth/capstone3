import {useState, useEffect} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import ProductCard from './ProductCard.js';


export default function UserProductView({productsData, noProducts}){
	const [products, setProducts] = useState([]);

	useEffect(() => {
		if(noProducts === false){
			setProducts(productsData.map(product_item => {
				return (
					(product_item.isActive !== false) ?
						<ProductCard key={product_item._id} product={product_item}/>
					:
						null	
				)
			}))
		}
	}, [noProducts, productsData])

	return (
		<Container>
			<Row>
				{noProducts === true ?
					<h1 className="text-center my-5">No Products Available</h1>	
				:
					<>
						<Col xs={12}>
							<h1 className="text-center my-5 main-title">Choose Your Drip</h1>
						</Col>
						{products}
					</>
				}
			</Row>
		</Container>
	)
}