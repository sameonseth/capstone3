import {useContext} from 'react';
import {Container, Navbar, Nav, NavDropdown} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import Login from './Login.js';
import UserContext from '../UserContext.js';


export default function AppNavbar() {
	const {user} = useContext(UserContext);

	return (
		<Navbar className="bg-dark sticky-top" expand="lg">
			<Container fluid className="px-5">
			    <Navbar.Brand className="lighter" as={Link} to="/">theDripShop</Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" className="navbar-toggler"/>
			    <Navbar.Collapse id="basic-navbar-nav">
			    	<Nav className="ms-auto gap-3">
			    		<Nav.Link as={NavLink} to="/">Home</Nav.Link>
			    		<Nav.Link as={NavLink} to="/products">
			    			{user.isAdmin === true ? "Admin Dashboard" : "Products"}
			    		</Nav.Link>
			    		{user.id !== null ?
			    			<>
			    				<NavDropdown className="" title={`Welcome, ${user.firstName}`} id="basic-nav-dropdown">
			                        <NavDropdown.Item className="lighter" as={NavLink} to="/profile">Profile</NavDropdown.Item>
			                        {user.isAdmin === false && ( 
				                        <NavDropdown.Item className="lighter" as={NavLink} to="/orders">Orders</NavDropdown.Item>
			                    	)}
			                        <NavDropdown.Item className="lighter" as={NavLink} to="/logout">Logout</NavDropdown.Item>
					            </NavDropdown>
		    				</>
			    		:
		    				<>
		    					<Login triggerElement="NavLink"/>
		    					<Nav.Link as={NavLink} to="/register">Sign Up</Nav.Link>
		    				</>
		    			}
			    	</Nav>
		    	</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}