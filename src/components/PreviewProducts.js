import {Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import * as images from '../images';


export default function Product(props) {
	const {data} = props;
	const {_id, name, description, image, price} = data;

	if(props)
		console.log(image);

	return (
		<Col xs={12} sm={{span: 8, offset: 0}} md={{span: 4, offset: 0}} lg={{span: 3, offset: 0}}>
			<Card className="cardHighlight mx-2">
				<Card.Img variant="top" src={images[`${image}`]} id="preview-product-img"/>
				<Card.Body className="bg-lighter">
					<Card.Title className="text-center">
						{name}
					</Card.Title>
					<Card.Text className="text-truncate">
						{description}
					</Card.Text>
					<h5 className="text-center text-">₱{price}</h5>
					<Link className="btn btn-primary d-block" to={`/products/${_id}`}>Details</Link>
				</Card.Body>
			</Card>
		</Col>
	)
}