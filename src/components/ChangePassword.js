import {useState, useEffect} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ChangePassword({old_password}){
	const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
	const [showModal, setShowModal] = useState(false);

	useEffect(() => {
        setIsActive((oldPassword && newPassword && confirmPassword) && (newPassword === confirmPassword));
	}, [old_password, oldPassword, newPassword, confirmPassword]);

    const changePassword = (event) => {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/new-password`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                old_password: oldPassword,
                new_password: newPassword
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data.message) return closeModal();
            if(data.status === "INCORRECT_PASSWORD"){
                Swal.fire({
                    title: "Failed",
                    icon: "error",
                    text: "Incorrect old password."
                });
            }
            else if(data.status === "OKAY"){
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: `Password changed successfully!`
                });
            } 
            closeModal();
        }).catch(error => console.log(error));
    }

    const closeModal = () => {
    	setOldPassword("");
        setNewPassword("");
        setConfirmPassword("");
        setShowModal(false);
    }

	return (
		<>
            <Button variant="primary" onClick={() => setShowModal(true)}>Change Password</Button>
			<Modal show={showModal} onHide={closeModal}>
				<Form onSubmit={(event) => changePassword(event)}>
                    <Modal.Header>
                    	<Modal.Title>Change Password</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
	                    <Form.Group controlId="oldPassword">
	                        <Form.Label>Old Password</Form.Label>
	                        <Form.Control type="password" placeholder="Enter Old Password" value={oldPassword} onChange={event => setOldPassword(event.target.value)} required/>
	                    </Form.Group>
                        <Form.Group controlId="newPassword">
                            <Form.Label>New Password</Form.Label>
                            <Form.Control type="password" placeholder="Enter New Password" value={newPassword} onChange={event => setNewPassword(event.target.value)} required/>
                        </Form.Group>
                        <Form.Group controlId="confirmPassword">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={event => setConfirmPassword(event.target.value)} required/>
                        </Form.Group>
                    </Modal.Body>  
                    <Modal.Footer>
                    	<Button variant="secondary" onClick={closeModal}>Close</Button>
                    	<Button variant="success" type="submit" disabled={!isActive}>Submit</Button>
                    </Modal.Footer>           
                </Form>
			</Modal>
		</>
	)
}