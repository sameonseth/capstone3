import {useContext, useState, useEffect} from 'react';
import {Button, Nav, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';


export default function Login({triggerElement, showAfterRegistration, setShowAfterRegistration}){
	const {setUser} = useContext(UserContext);
	const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
	const [showLogin, setShowLogin] = useState(false);

	useEffect(() => {
		setIsActive(email && password);
	}, [email, password]);

    useEffect(() => {
        if(triggerElement === "Register" && showAfterRegistration){
            setShowLogin(true);
            setShowAfterRegistration(false);
        }
    }, [triggerElement, showAfterRegistration, setShowAfterRegistration]);
   

	const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            setUser({
                id: data._id,
                firstName: data.firstName,
                isAdmin: data.isAdmin
            });
        });
    }

    function authenticate(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data.message) return closeEdit();
            if(data.status === "INVALID_USER"){
            	Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "User not found."
                });
            }
            else if(data.status === "INCORRECT_PASSWORD"){
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                });
            }
            else if(data.status === "OKAY"){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);
                Swal.fire({
                    title: "Login successful",
                    icon: "success",
                    text: `Welcome to theDripShop!`
                });
            } 
            closeEdit();
        })
    }

    const closeEdit = () => {
    	setEmail("");
        setPassword("");
        setShowLogin(false);
    }

	return (
		<>
            {triggerElement === "Button" ?
                <Button variant="primary" onClick={() => setShowLogin(true)}>Checkout</Button>
            :
                triggerElement === "NavLink" ?
                    <Nav.Link className="lighter" onClick={() => setShowLogin(true)}>Login</Nav.Link>
                :
                    null
            }
			<Modal show={showLogin} onHide={closeEdit}>
				<Form onSubmit={(event) => authenticate(event)}>
                    <Modal.Header>
                    	<Modal.Title>Login</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    	<Form.Group controlId="userEmail">
	                        <Form.Label>Email address</Form.Label>
	                        <Form.Control type="email" placeholder="Enter Email" value={email} onChange={event => setEmail(event.target.value)} required/>
	                    </Form.Group>
	                    <Form.Group controlId="password">
	                        <Form.Label>Password</Form.Label>
	                        <Form.Control type="password" placeholder="Enter Password" value={password} onChange={event => setPassword(event.target.value)} required/>
	                    </Form.Group>
                    </Modal.Body>  
                    <Modal.Footer>
                    	<Button variant="secondary" onClick={closeEdit}>Close</Button>
                    	<Button variant="success" type="submit" disabled={!isActive}>Submit</Button>
                    </Modal.Footer>           
                </Form>
			</Modal>
		</>
	)
}