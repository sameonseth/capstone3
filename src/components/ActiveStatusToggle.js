import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ActiveStatusToggle({product, fetchData}){
    const archiveToggle = (productId) => {
        console.log(productId);
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data.message) return;
            if(data.status === "OKAY"){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product archived successfully"
                });
                fetchData();
            }
            else if(data.status === "INVALID_PRODUCT"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Product does not exist."
                });
            }
        });
    }

    const activateToggle = (productId) => {
        console.log(productId);
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data.message) return;
            if(data.status === "OKAY"){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product activated successfully"
                });
                fetchData();
            }
            else if(data.status === "EMPTY_STOCK"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Cannot activate the product when the stock is zero."
                });
            }
            else if(data.status === "INVALID_PRODUCT"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Product does not exist."
                });
            }
        });
    }

	return (
        product.isActive ?
            <Button variant="danger" size="sm" onClick={() => archiveToggle(product.id)}>Archive</Button>
    	:	
            <Button variant="success" size="sm" onClick={() => activateToggle(product.id)}>Activate</Button>
	)
}