import {Container, Row, Col, Spinner} from 'react-bootstrap';


export default function Loading(){
	return(
		<Container>
			<Row>
				<Col xs={12} className="d-flex align-items-center justify-content-center min-vh-92">
					<Spinner animation="border" variant="secondary"/>
				</Col>
			</Row>
		</Container>
	)
}