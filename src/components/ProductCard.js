import PropTypes from 'prop-types';
import {Col, Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import * as images from '../images';


//destructured product property from props
export default function ProductCard({product}){
	const {_id, image, name} = product;

	return (
		<Col xs={12} md={{span: 8, offset: 2}} lg={{span: 6, offset: 0}} className="p-5 text-center">
			<h5>{name}</h5>
			<Link to={`/products/${_id}`}>
				<Image src={images[`${image}`]} className="products-img" fluid/>
			</Link>
		</Col>
	)
}


ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}