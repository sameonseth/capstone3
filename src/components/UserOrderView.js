import {useState, useEffect} from 'react';
import {Container, Row, Col, Accordion} from 'react-bootstrap';
import Loading from '../components/Loading.js';


export default function UserOrderView({ordersData, noOrders, loading}){
	const [orders, setOrders] = useState([]);
	const [updatedData, setUpdatedData] = useState([]);

	useEffect(() => {
		if(!loading){
			setUpdatedData(ordersData.map(order => (
				{
					_id: order._id,
					userId: order.userId,
					userFullName: order.userFullName,
					purchasedOn: new Date(order.purchasedOn),
					products: order.products,
					totalAmount: order.totalAmount	 
				}
			)));
		}
	}, [loading, ordersData]);

	useEffect(() => {
		console.log(updatedData);
		setOrders(updatedData.map((order, index) => {
			return (
				<Accordion.Item key={order._id} eventKey={order._id} >
		            <Accordion.Header className="accordion-header">
		            	<div>
		                	<div>
		                		Order #{updatedData.length-index} - Purchased on: {order.purchasedOn.getMonth()}/{order.purchasedOn.getDate()}/{order.purchasedOn.getFullYear()}, {order.purchasedOn.getHours()}:{order.purchasedOn.getMinutes()}:{order.purchasedOn.getSeconds()} (Click for Details)
		                	</div>
		              	</div>
		            </Accordion.Header>
		            <Accordion.Body className="accordion-body">
		            	<div>
			                Items:
			                <ul>
			                	{order.products.map(product => (
			                    	<li key={product.productId}>
			                      		{product.productName} - Quantity: {product.quantity}
			                    	</li>
			                  	))}
			                </ul>
			            </div>
		            	<div>Total: 
		            		<div>₱{order.totalAmount}</div>
		            	</div>
		            </Accordion.Body>
		        </Accordion.Item>
			)
		}));
	}, [updatedData]);

	if(loading) return <Loading/>

	return (
		!loading && (
			<Container>
				<Row>
					{noOrders === true ?
						<h1 className="text-center my-5">No Orders Available</h1>	
					:
						<Col xs={12}>
							<h1 className="text-center my-5 main-title">Order History</h1>
							<h5>Sorted by: <span className="text-success">Latest Checkout</span></h5>
							<Accordion className="table-responsive" defaultActiveKey={null}>
						    	{orders}
						    </Accordion>
						</Col>
					}
				</Row>
			</Container>
		)
	)
}