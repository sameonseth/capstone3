import {useState} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2'


export default function EditProduct({productId, fetchData}){
	const [image, setImage] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [stock, setStock] = useState("");
	const [showEdit, setShowEdit] = useState(false);

    const openEdit = () => {
    	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(response => response.json())
        .then(data => {
        	setShowEdit(true);
        	setImage(data.image);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setStock(data.stock);
        });
    }

    const closeEdit = () => {
    	setShowEdit(false);
    	setName("");
        setDescription("");
        setPrice(0);
        setStock(0);
    }

    const editProduct = (event) => {
    	event.preventDefault();
    	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                image: image,
                name: name,
                description: description,
                price: price,
                stock: stock
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data.message) return;
            if(data.status === "OKAY"){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product updated successfully!"
                });
            	closeEdit();
                fetchData();
            }
            else if(data.status === "INVALID_PRICE"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Invalid price value."
                });
            	closeEdit();
            }
            else if(data.status === "INVALID_STOCK"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Invalid stock value."
                });
            	closeEdit();
            }
            else if(data.status === "INVALID_PRODUCT"){
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Product does not exist."
                });
            	closeEdit();
            }
        });
    }

	return (
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit()}>Edit</Button>
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={(event) => editProduct(event)}>
                    <Modal.Header>
                    	<Modal.Title>Edit Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    	<Form.Group controlId="productImage">
	                        <Form.Label>Image Name</Form.Label>
	                        <Form.Control type="text" placeholder="Enter Image Name" value={image} onChange={event => setImage(event.target.value)} required/>
	                    </Form.Group>
                        <Form.Group controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter Name" value={name} onChange={event => setName(event.target.value)} required/>
                        </Form.Group>
	                    <Form.Group controlId="productDescription">
	                        <Form.Label>Description</Form.Label>
	                        <Form.Control type="text" placeholder="Enter Description" value={description} onChange={event => setDescription(event.target.value)} required/>
	                    </Form.Group>
	                    <Form.Group controlId="productPrice">
	                        <Form.Label>Price</Form.Label>
	                        <Form.Control type="number" placeholder="Enter Price" value={price} onChange={event => setPrice(event.target.value)} required/>
	                    </Form.Group>
	                    <Form.Group controlId="productStock">
	                        <Form.Label>Stock</Form.Label>
	                        <Form.Control type="number" placeholder="Enter Stock" value={stock} onChange={event => setStock(event.target.value)} required/>
	                    </Form.Group>
                    </Modal.Body>  
                    <Modal.Footer>
                    	<Button variant="secondary" onClick={closeEdit}>Close</Button>
                    	<Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>           
                </Form>
			</Modal>
		</>
	)
}