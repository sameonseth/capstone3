import React from 'react';

const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;

/*
import React, { createContext, useState, useEffect } from 'react';

const UserContext = createContext();

export function UserProvider({ children }) {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true); // Initial loading state

  useEffect(() => {
    // Fetch user data here
    fetchUserData()
      .then((userData) => {
        setUser(userData);
        setLoading(false); // Loading is complete
      })
      .catch((error) => {
        console.error('Error fetching user data:', error);
        setLoading(false); // Loading is complete even in case of error
      });
  }, []);

  return (
    <UserContext.Provider value={{ user, loading }}>
      {children}
    </UserContext.Provider>
  );
}

export default UserContext;

*/