export { default as banner_background } from './banner_background.jpg';
export { default as beanie_hat } from './beanie_hat.jpg';
export { default as denim_pants } from './denim_pants.jpg';
export { default as glasses } from './glasses.jpg';
export { default as gucci_bag } from './gucci_bag.jpg';
export { default as nike_shoes } from './nike_shoes.jpg';
export { default as tshirt } from './tshirt.jpg';