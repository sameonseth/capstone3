import {useContext, useState, useEffect, useCallback} from 'react';
import {Container, Row, Button, Table, Accordion} from 'react-bootstrap';
import {useNavigate, Navigate} from 'react-router-dom';
import ActiveStatusToggle from '../components/ActiveStatusToggle.js';
import AddProduct from '../components/AddProduct.js';
import AdminStatusToggle from '../components/AdminStatusToggle.js';
import EditProduct from '../components/EditProduct.js';
import Loading from '../components/Loading.js';
import UserContext from '../UserContext.js';


export default function AdminDashboard({showTable, setShowTable}){
	const {user, loading} = useContext(UserContext);
	const navigate = useNavigate();
	
	const [users, setUsers] = useState([]);
	const [products, setProducts] = useState([]);
	const [orders, setOrders] = useState([]);

	const [updatedOrderData, setUpdatedOrderData] = useState([]);

	const [noUsers, setNoUsers] = useState(false);
	const [noProducts, setNoProducts] = useState(false);
	const [noOrders, setNoOrders] = useState(false);
	
	const [showUsersTable, setShowUsersTable] = useState(false);
	const [showProductsTable, setShowProductsTable] = useState(true);
    const [showOrdersTable, setShowOrdersTable] = useState(false);

    const fetchUsersData = useCallback(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/all`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(data.message) return navigate("*");
			if(data.status === "EMPTY_USERS"){
				setNoUsers(true);
			}
			else{
				setNoUsers(false);
				setUsers(data.map(user => {
					return (
						<tr key={user._id}>
							<td>{user._id}</td>
				            <td>{user.firstName}</td>
				            <td>{user.lastName}</td>
				            <td>{user.email}</td>
				            <td>{user.mobileNo}</td>
				            {
								user.isAdmin !== false ?
									<td className="text-success">Admin</td>
								:
									<td className="text-danger">User</td>
				            }
				            <td><AdminStatusToggle user={{id: user._id, isAdmin: user.isAdmin}} fetchData={fetchUsersData}/></td>
			            </tr>
					)
				}));
			}
		});
	}, [navigate]);

	const fetchProductsData = useCallback(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(data.message) return navigate("*");
			if(data.status === "EMPTY_PRODUCTS"){
				setNoProducts(true);
			}
			else{
				setNoProducts(false);
				setProducts(data.map(product => {
					return (
						<tr key={product._id}>
							<td>{product._id}</td>
				            <td>{product.name}</td>
				            <td>{product.description}</td>
				            <td>{product.price}</td>
				            <td>{product.stock}</td>
				            {
								product.isActive !== false ?
									<td className="text-success">Available</td>
								:
									<td className="text-danger">Unavailable</td>
				            }
				            <td><EditProduct productId={product._id} fetchData={fetchProductsData}/></td>
				            <td><ActiveStatusToggle product={{id: product._id, isActive: product.isActive}} fetchData={fetchProductsData}/></td>
			            </tr>
					)
				}));
			}
		});
	}, [navigate]);

	const fetchOrdersData = useCallback(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(data.status === "EMPTY_ORDERS") setNoOrders(true);
			else{
				setNoOrders(false);
				setUpdatedOrderData(data.map(order => (
					{
						_id: order._id,
						userId: order.userId,
						userFullName: order.userFullName,
						purchasedOn: new Date(order.purchasedOn),
						products: order.products,
						totalAmount: order.totalAmount	 
					}
				)));
				setOrders(updatedOrderData.map((order, index) => {
					return (
						<Accordion.Item key={order._id} eventKey={order._id} >
				            <Accordion.Header className="accordion-header">
				            	<div>
				                	<div>
				                		Order #{updatedOrderData.length-index} - Purchased on: {order.purchasedOn.getMonth()}/{order.purchasedOn.getDate()}/{order.purchasedOn.getFullYear()}, {order.purchasedOn.getHours()}:{order.purchasedOn.getMinutes()}:{order.purchasedOn.getSeconds()} (Click for Details)
				                	</div>
				              	</div>
				            </Accordion.Header>
				            <Accordion.Body className="accordion-body">
				            	<div>UserID: {order.userId}</div>
				            	<div>Name: {order.userFullName}</div>
				            	<div>
					                Items:
					                <ul>
					                	{order.products.map(product => (
					                    	<li key={product.productId}>
					                      		{product.productName} - Quantity: {product.quantity}
					                    	</li>
					                  	))}
					                </ul>
					            </div>
				            	<div>Total: ₱{order.totalAmount}</div>
				            </Accordion.Body>
				        </Accordion.Item>
					)
		        }));
			}
		});
	}, [updatedOrderData]);

	useEffect(() => {
		if(!loading){
			if(showUsersTable) fetchUsersData();
			else if(showProductsTable) fetchProductsData();
			else if(showOrdersTable) fetchOrdersData();
		}
	}, [loading, showUsersTable, showProductsTable, showOrdersTable, fetchUsersData, fetchProductsData, fetchOrdersData]);

	const handleShowUsers = () => {
        setShowProductsTable(false);
        setShowUsersTable(true);
        setShowOrdersTable(false);
    };

	const handleShowProducts = () => {
        setShowProductsTable(true);
        setShowUsersTable(false);
        setShowOrdersTable(false);
    };

    const handleShowOrders = () => {
        setShowProductsTable(false);
        setShowUsersTable(false);
        setShowOrdersTable(true);
    };

    useEffect(() => {
    	console.log(showTable);
    	if(showTable === "Users"){
    		setShowTable("");
    		handleShowUsers();
    	}
    	else if(showTable === "Products"){
    		setShowTable("");
    		handleShowProducts();
    	}
    	else if(showTable === "Orders"){
    		setShowTable("");
    		handleShowOrders();
    	}
    }, [showTable, setShowTable]);

    if(loading) return <Loading/>

	return (
		!loading && (
			(user.id === null || user.isAdmin === false) ?
				<Navigate to="/products" />
			:
				<Container fluid>
					<Row>
						<h1 className="text-center my-5">Admin Dashboard</h1>
						<div className="d-flex justify-content-center gap-3">
							<Button variant="primary" size="sm" onClick={handleShowUsers} disabled={showUsersTable}>Users</Button>
							<Button variant="primary" size="sm" onClick={handleShowProducts} disabled={showProductsTable}>Products</Button>
							<Button variant="primary" size="sm" onClick={handleShowOrders} disabled={showOrdersTable}>Orders</Button>
		                </div>
		                {showUsersTable && (
		                	<>
			                	{noUsers === true ?
			                		<h2 className="text-center my-5">No Users Available</h2>
			                	:
				                    <Table className="mt-3" striped bordered hover responsive>
										<thead className="text-center">
									        <tr>
									          	<th>UserID</th>
									          	<th>First Name</th>
									          	<th>Last Name</th>
									          	<th>Email Address</th>
									          	<th>Mobile Number</th>
									          	<th>Role</th>
									          	<th colSpan={2}>Actions</th>
									        </tr>
										</thead>
										<tbody>
											{users}
										</tbody>
							    	</Table>
							    }
							</>
		                )}
		                {showProductsTable && (
		                	<>
			                	<AddProduct fetchData={fetchProductsData}/>
			                	{noProducts === true ?
									<h2 className="text-center my-5">No Existing Products</h2>
								:
									<Table className="mt-3" striped bordered hover responsive>
										<thead className="text-center">
									        <tr>
									          	<th>ID</th>
									          	<th>Name</th>
									          	<th>Description</th>
									          	<th>Price</th>
									          	<th>Stock</th>
									          	<th>Availability</th>
									          	<th colSpan={2}>Actions</th>
									        </tr>
										</thead>
										<tbody>
											{products}
										</tbody>
							    	</Table>
								}
						    </>
		                )}
		                {showOrdersTable && (
					    	<>
					    		{noOrders === true ?
			                		<h2 className="text-center my-5">No Orders Available</h2>
			                	:
			                		<>
			                			<h5 className="mt-5">Sorted by: <span className="text-success">Latest Checkout</span></h5>
				                		<Accordion className="table-responsive">
									    	{orders}    
									    </Accordion>
									</>
			                	}
			                </>
		                )}
	                </Row>
			    </Container>
		)
	)
}