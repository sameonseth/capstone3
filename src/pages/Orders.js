import {useContext, useState, useCallback, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import Loading from '../components/Loading.js';
import UserOrderView from '../components/UserOrderView.js';
import AdminDashboard from './AdminDashboard.js';
import UserContext from '../UserContext.js';


export default function Orders(){
	const {user, loading} = useContext(UserContext);
	const [showTable, setShowTable] = useState("Orders");
	const [orders, setOrders] = useState([]);
	const [ordersLoading, setOrdersLoading] = useState(true);
	const [noOrders, setNoOrders] = useState([]);

	const fetchData = useCallback(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/`, {
			headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(data.status === "EMPTY_PRODUCTS") setNoOrders(true);
			else{
				setNoOrders(false);
				setOrders(data);
			}
			setOrdersLoading(false);
		})
		.catch(error => {
			console.log(`Error: ${error}`);
			setOrdersLoading(false);
		});
	}, []);

	useEffect(() => {
		if(!loading) fetchData();
	}, [loading, fetchData]);

	if(loading) return <Loading/>

	return (
		!loading && user.id ?
			user.isAdmin ?
			 	<AdminDashboard showTable={showTable} setShowTable={setShowTable}/>
			:
				<UserOrderView ordersData={orders} noOrders={noOrders} loading={ordersLoading}/>
		:
			<Navigate to="/" />
	)
}

