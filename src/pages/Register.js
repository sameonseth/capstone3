import {useContext, useState, useEffect} from 'react';
import {Container, Row, Col, Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import Loading from '../components/Loading.js';
import Login from '../components/Login.js';
import UserContext from '../UserContext.js';


export default function Register(){
    const {user, loading} = useContext(UserContext);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);
    const [showAfterRegistration, setShowAfterRegistration] = useState(false);

    useEffect(() => {
        setIsActive((firstName && lastName && email && mobileNo && password && confirmPassword) && (password === confirmPassword) && (mobileNo.length === 11));
    }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

    function registerUser(event){
        event.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
            })
        }).then(response => response.json()).then(data => {
  			console.log(data);
        	if(data.message) return;
            if(data.status === "REGISTERED_USER"){
            	Swal.fire({
                    title: "Registration failed",
                    icon: "error",
                    text: "User is already registered."
                });
            }
            else if(data.status === "OKAY"){
            	Swal.fire({
                    title: "Registration successful",
                    icon: "success",
                    text: "User registered successfully!"
                });	

                setFirstName("");
                setLastName("");
                setEmail("");
                setMobileNo("");
                setPassword("");
                setConfirmPassword("");
                setShowAfterRegistration(true);
            }
        });
    };

    if(loading) return <Loading/>

	return (
        !loading && (
            (user.id !== null) ?
                <Navigate to="/products" />       
            :
                <>
                    <Login triggerElement="Register" showAfterRegistration={showAfterRegistration} setShowAfterRegistration={setShowAfterRegistration}/>
                    <Container>
                        <Row>
                            <Col xs={12} md={{span: 8, offset: 2}}>
                                <h1 className="my-5 text-center main-title">Sign Up</h1>
                    		    <Form onSubmit={(event) => registerUser(event)} className="d-flex flex-column gap-3">
                                    <Form.Group>
                                        <Form.Label>First Name:</Form.Label>
                                        <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={event => setFirstName(event.target.value)} required/>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Last Name:</Form.Label>
                                        <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={event => setLastName(event.target.value)} required/>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Email:</Form.Label>
                                        <Form.Control type="email" placeholder="Enter Email" value={email} onChange={event => setEmail(event.target.value)} required/>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Mobile No:</Form.Label>
                                        <Form.Control type="number" placeholder="Enter 11 Digit No." value={mobileNo} onChange={event => setMobileNo(event.target.value)} required/>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Password:</Form.Label>
                                        <Form.Control type="password" placeholder="Enter Password" value={password} onChange={event => setPassword(event.target.value)} required/>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Confirm Password:</Form.Label>
                                        <Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={event => setConfirmPassword(event.target.value)} required/>
                                    </Form.Group>
                                    <Button variant="primary" type="submit" disabled={!isActive}>Submit</Button>
                                </Form>
                            </Col>
                        </Row>
                    </Container>
                </>
        )
	)
}