import {useContext, useState, useEffect} from 'react';
import {Container, Row, Col, Card} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import ChangePassword from '../components/ChangePassword.js';
import Loading from '../components/Loading.js';
import UserContext from '../UserContext.js';


export default function Profile(){
    const {user, loading} = useContext(UserContext);
	const [details, setDetails] = useState({});

    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data)
            setDetails(data);
        }); 
    }

    useEffect(() => {
    	if(!loading) fetchData();
	}, [loading]);

    if(loading) return <Loading/>

    return (
    	!loading && (
			user.id === null ?
				<Navigate to="/" />
			:
				<Container fluid>
					<Row>
						<Col xs={12} md={{span: 4, offset: 4}} className="mb-4">
							<h1 className="text-center mt-5 mb-4 main-title">My Profile</h1>
							<Card className="cardHighlight">
								<Card.Body className="border-dark d-flex flex-column gap-5 p-5">
									<Card.Title className="text-center">
										<h2>{`${details.firstName} ${details.lastName}`}</h2>
									</Card.Title>
									<Card.Text className="text-truncate text-center">
										<h3>Contacts</h3>
										<div>Email: {details.email}</div>
										<div>Mobile No: {details.mobileNo}</div>
									</Card.Text>
									<ChangePassword old_password={details.password}/>
								</Card.Body>
							</Card>
						</Col>
					</Row>
				</Container>
		)
	)
}