import {useContext, useState} from 'react';
import {Navigate} from 'react-router-dom';
import Loading from '../components/Loading.js';
import AdminDashboard from './AdminDashboard.js';
import UserContext from '../UserContext.js';


export default function Users(){
	const {user, loading} = useContext(UserContext);
	const [showTable, setShowTable] = useState("Users");

	if(loading) return <Loading/>
	
	return (
		!loading && (
			user.isAdmin === true ?
				<AdminDashboard showTable={showTable} setShowTable={setShowTable}/>
			:
				<Navigate to="/" />	
		)
	)
}

