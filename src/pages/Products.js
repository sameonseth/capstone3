import {useContext, useState, useCallback, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import Loading from '../components/Loading.js';
import UserProductView from '../components/UserProductView.js';
import AdminDashboard from './AdminDashboard.js';
import UserContext from '../UserContext.js';


export default function Products(){
	const {user, loading} = useContext(UserContext);
	const navigate = useNavigate();
	const [showTable, setShowTable] = useState("Products");
	const [products, setProducts] = useState([]);
	const [noActiveProducts, setNoActiveProducts] = useState(false);

	const fetchData = useCallback(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(data.message) return navigate("*");
			if(data.status === "EMPTY_PRODUCTS") setNoActiveProducts(true);
			else{
				setNoActiveProducts(false);
				setProducts(data);
			}
		});
	}, [navigate]);

	useEffect(() => {
		if(!loading) fetchData();
	}, [loading, fetchData]);

	if(loading) return <Loading/>

	return (
		!loading && (
			(user.isAdmin === true) ?
			 	<AdminDashboard showTable={showTable} setShowTable={setShowTable}/>
			:
				<UserProductView productsData={products} noProducts={noActiveProducts}/>
		)
	)
}

