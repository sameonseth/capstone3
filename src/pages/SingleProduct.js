import {useContext, useState, useCallback, useEffect} from 'react';
import {Container, Row, Col, Image, Card, InputGroup, FormLabel, FormControl, Button} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import Loading from '../components/Loading.js';
import Login from '../components/Login.js';
import * as images from '../images';
import UserContext from '../UserContext.js';


export default function SingleProduct() {
	const {user, loading} = useContext(UserContext);
	const {productId} = useParams();
	const navigate = useNavigate();
	const [isActive, setIsActive] = useState(false);
	const [image, setImage] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [quantity, setQuantity] = useState(0);

	const decreaseQuantity = () => {
	    if(quantity >= 1){
	    	setQuantity(quantity - 1);
	    }
	}

	const increaseQuantity = () => {
		setQuantity(quantity + 1);
	}

	const fetchData = useCallback(() => {
		console.log(productId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(data.message) return navigate("*");
			setIsActive(data.isActive);
			setImage(data.image);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});
	}, [productId, navigate]);

	useEffect(() => {
		if(!loading) fetchData();
	}, [loading, fetchData]);

	const checkout = (event, productId) => {
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/orders/new-order`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				products: [
					{
						productId: productId,
						quantity: quantity
					}
				]
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			if(data.message) return;
			if(data.status === "INVALID_PRODUCT"){
				Swal.fire({
                    title: "Oops",
                    icon: "error",
                    text: "Product is not available."
                });
			}
			else if(data.status === "INVALID_QUANTITY"){
				Swal.fire({
                    title: "Oops",
                    icon: "error",
                    text: "Invalid quantity value."
                });
			}
			else if(data.status === "LIMITED_STOCK"){
				Swal.fire({
                    title: "Oops",
                    icon: "error",
                    text: "We don't have enough of that product."
                });
			}
			else if(data.status === "OKAY"){
				Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Product checkout is successful!"
                });
                fetchData();
                setQuantity(0);
			}
		});
	}

	if(loading) return <Loading/>

	return (
		<Container className="mt-5">
			<Row>
				<Col xs={12} md={6}>
					<Image src={images[`${image}`]} fluid />
				</Col>
				<Col xs={12} md={6}>
					<Card className="cardHighlight">
		    			<Card.Body>
							<Card.Title>{name}</Card.Title>
							{isActive === true ?
								<Card.Text className="text-success">Available</Card.Text>
							:
								<Card.Text className="text-danger">Unavailable</Card.Text>
							}
		        			<Card.Text>{description}</Card.Text>
		        			<Card.Subtitle>Price:</Card.Subtitle>
		        			<Card.Text>₱{price}</Card.Text>
		    			</Card.Body>
		    			<div className="text-center" id="card-footer">
		    				<FormLabel>Quantity:</FormLabel>
		    				<InputGroup className="mx-auto" id="quantity-input-group">
							    <Button variant="outline-secondary" onClick={() => decreaseQuantity()}>-</Button>
							    <FormControl className="text-center" id="quantity-field" type="number" value={quantity} onChange={event => setQuantity(event.target.value)}/>
							    <Button variant="outline-secondary" onClick={() => increaseQuantity()}>+</Button>
		        			</InputGroup>
		        			<div className="my-4">
			        			{!loading && (
			        				user.id !== null ?
			        					<Button variant="primary" onClick={(event) => checkout(event, productId)} disabled={user.isAdmin}>Checkout</Button>
			        				:
				        				<Login triggerElement="Button"/>
				        			)
		        				}
	        				</div>
		    			</div>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}