import {Container} from 'react-bootstrap';
import Banner from '../components/Banner.js';
import FeaturedProducts from '../components/FeaturedProducts.js';
//import Highlights from '../components/Highlights.js';


export default function Home(){
	const data = {
		parent: "home",
		title: "theDripShop",
		content: "Elevate Your Style with Trendy Clothing, Footwear, and Accessories",
		destination: "/products",
		label: "Shop Now"
	}

	return (
		<Container fluid>
			<Banner data={data}/>
			<FeaturedProducts/>
			{/*<Highlights/>*/}
		</Container>
	)
}